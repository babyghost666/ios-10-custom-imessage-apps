//
//  CustomBrowserViewController.swift
//  CustomStickerPack
//
//  Created by Peter Leung on 8/9/2016.
//  Copyright © 2016 winandmac Media. All rights reserved.
//

import UIKit
import Messages
import Foundation

class CustomBrowserViewController: MSStickerBrowserViewController {
    
    var stickers = [MSSticker]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func StartStickers(){
        for i in 1...3 {
            LoadSomeStickers(asset: "test\(i)", localDescript: "Testing TimCook")
        }
    }
    
    override func numberOfStickers(in stickerBrowserView: MSStickerBrowserView) -> Int {
        return stickers.count
    }
    
    override func stickerBrowserView(_ stickerBrowserView: MSStickerBrowserView, stickerAt index: Int) -> MSSticker {
        return stickers[index]
    }

    func LoadSomeStickers(asset:String, localDescript:String){
        guard let stickerPath = Bundle.main.path(forResource: asset, ofType: "gif")
        else {
            print("error")
            return
        }
        
        let stickerURL = URL(fileURLWithPath: stickerPath)
        
        let OneSticker: MSSticker
        do {
            try OneSticker = MSSticker(contentsOfFileURL: stickerURL, localizedDescription: localDescript)
            stickers.append(OneSticker)
            
        }catch{
            return
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
